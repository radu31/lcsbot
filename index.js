/**
 * @author Greenblud
 * @license MIT
 * @file index.js
 */

const { Client, Collection } = require("discord.js");
const { readdirSync } = require("fs");
const { sep } = require("path");

const { success, error, warning } = require("log-symbols");
const config = require("./config");

const bot = new Client({ autoReconnect: true });
bot.config = config;

// leagueapiwrapper -> https://github.com/LionelBergen/MundoScript
// let LeagueAPI = require("leagueapiwrapper");
// LeagueAPI = new LeagueAPI(bot.config.leagueAPIKey, Region.NA);

const randResponses = require("./textStuff.json");

["commands", "aliases"].forEach((x) => (bot[x] = new Collection()));

const load = (dir = "./commands/") => {
	readdirSync(dir).forEach((dirs) => {
		const commands = readdirSync(`${dir}${sep}${dirs}${sep}`).filter(
			(files) => files.endsWith(".js")
		);

		for (const file of commands) {
			const pull = require(`${dir}/${dirs}/${file}`);
			if (
				pull.help &&
				typeof pull.help.name === "string" &&
				typeof pull.help.category === "string"
			) {
				if (bot.commands.get(pull.help.name))
					return console.warn(
						`${warning} Deux ou plusieurs commandes ont le même nom: ${pull.help.name}.`
					);
				bot.commands.set(pull.help.name, pull);
				console.log(`${success} Commande ${pull.help.name} chargée.`);
			} else {
				console.log(
					`${error} Erreur en chargeant la commande dans ${dir}${dirs}. Vous avez un help.name ou help.name qui est pas un string, ou vous avez une help.category ou help.category qui est pas un string`
				);
				continue;
			}
			if (pull.help.aliases && typeof pull.help.aliases === "object") {
				pull.help.aliases.forEach((alias) => {
					if (bot.aliases.get(alias))
						return console.warn(
							`${warning} Deux ou plusieurs commandes ont le même alias: ${alias}`
						);
					bot.aliases.set(alias, pull.help.name);
				});
			}
		}
	});
};

// loading commands
load();

// listeners
bot.on("ready", () => {
	console.log(
		`[START] I'm logged in as ${bot.user.username} (${bot.user.id})`
	);
	bot.user.setActivity("yo help", { type: "LISTENING" }).catch(console.error);
});

bot.on("guildBanAdd", (guild, user) => {
	console.log(`[BAN] ${user.tag} a été ban de ${guild}`);
});

bot.on("guildBanRemove", (guild, user) => {
	console.log(`[BAN] Le ban de ${user.tag} a été retiré de ${guild}`);
});

bot.on("guildUnavailable", (guild) => {
	console.log(`[ERROR] Le serveur ${guild} est indisponible`);
});

bot.on("messageDelete", (message) => {
	const mtn = new Date();
	console.log(
		`[MsgDelete] Le message de ${message.author.username} a été créé à ` +
			`${message.createdAt.getHours()}:${message.createdAt.getMinutes()} ` +
			`et supprimé à ${mtn.getHours()}:${mtn.getMinutes()}\r\n\t${message}`
	);
});

bot.on("reconnecting", () => console.log("[INFO] Je me reconnecte..."));
bot.on("disconnect", () => {
	console.log("[INFO] Déconnecté, je redémarre...");
	process.exit();
});
// // NodeJS process listeners
process.on("unhandledRejection", console.error);
process.on("warning", console.warn);

bot.on("message", async (message) => {
	const prefix = bot.config.prefix;
	const args = message.content.slice(prefix.length).trim().split(/ +/g);
	const cmd = args.shift().toLowerCase();

	let command;
	if (message.author.bot || !message.guild) return;

	if (!message.member)
		message.member = await message.guild.fetchMember(message.author);

	if (!message.content.startsWith(prefix)) return;

	if (cmd.length === 0) {
		message.reply("Yo quess ya patnai?");
	}
	if (bot.commands.has(cmd)) command = bot.commands.get(cmd);
	else if (bot.aliases.has(cmd))
		command = bot.commands.get(bot.aliases.get(cmd));

	console.log(
		`[INFO] (${message.author.username}) exécution de la commande ${cmd} avec paramètres ${args}`
	);
	if (command) command.run(bot, message, args);
});

bot.login(bot.config.token).catch(function () {
	console.log("[INFO] oops");
});
