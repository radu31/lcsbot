const config = require("../config");
const version = "10.6.1";

const k = require("kayn");

/*
const redisCache = new RedisCache({
    host: 'localhost',
    port: 5000,
    keyPrefix: 'kayn',
});
*/
const myCache = new k.LRUCache({ max: 5 });

const kayn = k.Kayn(config.ritokey)({
	region: "na",
	debugOptions: {
		isEnabled: true,
		showKey: false,
	},
	requestOptions: {
		shouldRetry: true,
		numberOfRetriesBeforeAbort: 3,
		delayBeforeRetry: 1000,
	},
	cacheOptions: {
		cache: new k.BasicJSCache(),
	},
});

const main = async () => {
	// get last games of summoner
	// let account = await kayn.Summoner.by.name("Greenblud");
	// console.log(await kayn.Matchlist.by.accountID(account.accountId));

	// get current game todo
	// let account = kayn.Summoner.by.name("Greenblud");
	// console.log(await kayn.CurrentGame.by.summonerID(account.id));

	// get game
	let account = await kayn.Summoner.by.name("Greenblud");
	let matches = await kayn.Matchlist.by.accountID(account.accountId);
	// first index is the # game object
	let game = matches.matches[0];
	let match = await kayn.Match.get(game.gameId);
	let players = match.participantIdentities;
	// first index is the # summoner object
	console.log(players[0].player.summonerName);

	// get game id
};
main();
