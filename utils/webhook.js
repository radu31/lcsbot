const Discord = require("discord.js");
const fs = require("fs");
const config = require("../config");
const joueurs = require("./webhookppl");
const players = fs.readFileSync("./webhookppl.json");
const champs = fs.readFileSync("./championsIdToName.json");
const k = require("kayn");

const bibiland = new Discord.WebhookClient(
	"693433682924404747",
	"PJHczqbh6d4pqn7KQlwxYtlOJmtWK2pmL5fsj4SyvOJLNjFd9kv1ztqQFSElhSUuypob"
);
const test = new Discord.WebhookClient(
	"693435423577341952",
	"-MDUDdeQLw9jgAzvRotFMHnAxoicnA_AiIL6_uNjtXIYz_NkFHVB1k3NYNPwsh-ktOIV"
);

const myCache = new k.LRUCache({ max: 5 });

const kayn = k.Kayn(config.ritokey)({
	region: "na",
	debugOptions: {
		isEnabled: true,
		showKey: false,
	},
	requestOptions: {
		shouldRetry: true,
		numberOfRetriesBeforeAbort: 3,
		delayBeforeRetry: 1000,
	},
	cacheOptions: {
		cache: new k.BasicJSCache(),
	},
});

/*
    @qqn est mort ? fois durant sa derniere game

    si l'id de la derniere game de qqn est différente de celle dans le json, envoyer msg / webhook  DONE
    si summname est dans l'array badppl, send games with bad kda                                    TODO
    if summname in goodppl, send games with positive kda else not at all                            TODO
*/

let main = async function () {
	// "<@" + webhookppl[i].id + ">"
	// for(person in joueurs.ppl){
	// console.log(joueurs.ppl[0].id);
	// joueurs=JSON.parse(joueurs);

	// iterating through players objects in json
	for (let i = 0; i < joueurs.ppl.length; i++) {
		var j = new Array(joueurs.ppl.length);
		var str = "[" + joueurs.ppl[i].summonerName + "] ";
		var currentSum = "";
		// if(joueurs.ppl[i].id==""){
		//     joueurs.ppl[i].id = getaccountid(joueurs.ppl[i].summonerName);
		//     fs.writeFileSync("./webhookppl.json", JSON.stringify(joueurs, null, 4));
		// }
		// games list (to index 100)
		var matches = await kayn.Matchlist.by.accountID(joueurs.ppl[i].id);
		// (last) object game from matches (only for gameId)
		var game = matches.matches[0];
		var match;

		if (joueurs.ppl[i].lastGameId != game.gameId) {
			// details about a particular game
			match = await kayn.Match.get(game.gameId);
			joueurs.ppl[i].lastGameId = game.gameId;
			fs.writeFileSync(
				"./webhookppl.json",
				JSON.stringify(joueurs, null, 4)
			);

			// find identity of wanted summoner
			let tmpParticipantId = 0;
			for (var k = 0; k < match.participantIdentities.length; k++) {
				for (var l = 0; l < joueurs.ppl.length; l++) {
					if (
						match.participantIdentities[k].player.summonerName ==
							joueurs.ppl[l].summonerName &&
						match.participantIdentities[k].player.summonerName !=
							currentSum
					) {
						currentSum =
							match.participantIdentities[k].player.summonerName;
						tmpParticipantId =
							match.participantIdentities[k].participantId;
					}
				}
			}

			j[i] = match.participants[tmpParticipantId - 1].stats;
			let pentas = j[i].pentaKills,
				quadras = j[i].quadraKills,
				kills = j[i].kills,
				deaths = j[i].deaths,
				assists = j[i].assists,
				dmgToChamps = j[i].totalDamageDealtToChampions,
				visionScore = j[i].visionScore,
				wardsPlaced = j[i].wardsPlaced,
				wardsKilled = j[i].wardsKilled,
				minions = j[i].totalMinionsKilled,
				firstBlood = j[i].firstBloodKill,
				firstTower = j[i].firstTowerKill || j[i].firstTowerAssist;

			let getchamp = JSON.parse(champs, (key, value) => {
				return value;
			})[match.participants[tmpParticipantId - 1].championId];

			let getdiscid = JSON.parse(players, (key, value) => {
				return value;
			}).ppl[i].discordId;

			str += pentas
				? " haha il a fait " + pentas + " penta, esti qu'y est bon"
				: quadras
				? " oh wow, " + quadras + " quadra c'est malade"
				: deaths > 6
				? " ah man... il est mort " +
				  deaths +
				  " fois en jouant " +
				  getchamp +
				  ", criss de poche."
				: " lmao";
			str +=
				(kills + assists) / deaths > 4
					? " woah " +
					  ("<@" + getdiscid + ">") +
					  " a eu un kda de " +
					  (kills + assists) / deaths +
					  " durant sa dernière partie"
					: " lul " +
					  ("<@" + getdiscid + ">") +
					  " t'es un esti de poche avec ton " +
					  (kills + assists) / deaths +
					  " de kda.";
			str +=
				dmgToChamps > 25000
					? " dam boi t'es siboirement bon, t'as fait " +
					  dmgToChamps +
					  " dommages aux champions."
					: " hahalol";
			str +=
				visionScore > 40
					? " wow t'as trop bien ward! t'avais " +
					  visionScore +
					  " de vision score et t'as placé " +
					  wardsPlaced +
					  " wards en en tuant " +
					  wardsKilled
					: " hehe";
			str +=
				minions > 150
					? " kiet, t'aurais pu faire mieux avec ton cs de " + minions
					: firstBlood
					? " wow! t'as fait ca tout seul? t'as eu le first blood esti de fif."
					: firstTower
					? " woah première tour. t'es vrm bon."
					: " ._. ";
			test.send(str);
			// bibiland.send(str);
		}
	}
};
main();

let getaccountid = async function (name) {
	let account = await kayn.Summoner.by.name(name);
	console.log(
		"[INFO] Getting account id for " + name + ": " + account.accountId
	);
	// return account.id;
};

// let matches = kayn.Matchlist.by.accountID(joueurs.ppl[0].id);
// // let game = matches[0];
// // let match = kayn.Match.get(game.gameId);
// // let stats = match.participants[0].stats;
// console.log(matches);
// // console.log(game);
// // console.log(match);
// // console.log(stats);

// {
//     "summonerName": "Stop Write Meow",
//         "discordId": "275428645076467715",
//             "id": "giqE9NMza9LTRknJR5gJfZWpRZhK7yZ8OuCZ2OVoSQM",
//                 "lastGameId": 3352252637
// },
// {
//     "summonerName": "ashrakler",
//         "discordId": "269036808451391498",
//             "id": "u4lUMNylbbspwfGQXp3eyVLLxkvQ-Ktqga42XW6xqaIDnBM",
//                 "lastGameId": 2845973356
// },
// {
//     "summonerName": "Le Con",
//         "discordId": "346119696535519253",
//             "id": "Er4uKgWuH6dhaycmFVP_uoGQrW2JrKIPXWsLeLdL08HR0KI",
//                 "lastGameId": 3353653276
// },
// {
//     "summonerName": "Abdes",
//         "discordId": "346119696535519253",
//             "id": "6kOX9dUuZQa58JiTDcIL1qDNuJoUaPdzZ1q148qyPqs9vmA",
//                 "lastGameId": 3348825597
// },

// test
let testt = async function () {
	matches = await kayn.Matchlist.by.accountID(joueurs.ppl[0].id); // greenblud
	let game = matches.matches[0];
	let match = await kayn.Match.get(game.gameId);
	// console.log(matches);
	// console.log(game);
	// console.log(match);
	// console.log(match.participants[0].stats);
};
// testt();

// let test = (async function () {
//     // test.send('Yeet madafaka');
//     let account = await kayn.Summoner.by.name("Greenblud");
//     let matches = await kayn.Matchlist.by.accountID(account.accountId);
//     // first index is the # game object
//     let game = matches.matches[0];
//     let match = await kayn.Match.get(game.gameId);
//     let players = match.participantIdentities;
//     // first index is the # summoner object
//     // console.log(players[0].player.summonerName);
//     console.log(match);

//     // console.log(`${}`);
// });

// getaccountid("Greenblud");          // l7Ec1RnDiO1XNoX44tj3Gct2K74BrRriH3jfjlcdkCdNqWw
// getaccountid("Stop Write Meow");    // giqE9NMza9LTRknJR5gJfZWpRZhK7yZ8OuCZ2OVoSQM
// getaccountid("Daimonz");            // 06awGQ64foPL00usAuYh6SQjNoehyUfNZQ6M5CsWMV85s_I
// getaccountid("Dr Faiz");            // AKbLffDTl3rNiQ6BX83WuS1YsnnhjwC5VJk8XpOZB3_hEw
// getaccountid("Abdes");              // 6kOX9dUuZQa58JiTDcIL1qDNuJoUaPdzZ1q148qyPqs9vmA
// getaccountid("Le Con");             // Er4uKgWuH6dhaycmFVP_uoGQrW2JrKIPXWsLeLdL08HR0KI
// getaccountid("ashrakler");          // u4lUMNylbbspwfGQXp3eyVLLxkvQ-Ktqga42XW6xqaIDnBM
// getaccountid("Le Surveillant");     // WeZ9GRxzQSH527vpIybK7CpesmutTvTpHJ8UJzTTuC_GV4UZO9Hz_7Wz
//getaccountid("itsfactor");           // oT0lgpNPm5NfAQ46trRxS94nw-oBz885bYl95OQj4kk2A8o
