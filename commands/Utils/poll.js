/* TODO: poll anonyme
	send msg with question to user and collect reaction data
*/
const Discord = require("discord.js");

module.exports.run = async (bot, message, args) => {
	const embed = new Discord.MessageEmbed();
	if (args.length < 2)
		return message.channel.send(
			"Vous devez fournir plus de " +
				"paramètres que ça.. utilisez la commande help pour vous aider."
		);
	else if (
		[
			"yesno",
			"noyes",
			"ouinon",
			"nonoui",
			"ouiounon",
			"yesorno",
			"binaire",
			"oui/non",
			"yes/no",
			"binary",
		].includes(args[0])
	) {
		embed
			.setColor("#3333cc")
			.setTitle(`${args.slice(1).join(" ")}`)
			.setAuthor(`${bot.user.username}`, bot.user.displayAvatarURL)
			.setFooter(
				`Demandé par ${message.author.tag}`,
				message.author.displayAvatarURL
			)
			.setTimestamp();
		console.log("yesno");
		message.channel.send(embed).then((messageReaction) => {
			// messageReaction.react("👍");
			// messageReaction.react("👎");
			messageReaction.react("✅");
			messageReaction.react("❎");
		});
	} else if (
		[
			"list",
			"liste",
			"choose",
			"pick",
			"choix",
			"réponses",
			"reponses",
			"réponse",
			"reponse",
			"choice",
		].includes(args[0])
	) {
		// let choices = ["0⃣", "1⃣", "2⃣", "3⃣", "4⃣", "5⃣", "6⃣", "7⃣", "8⃣", "9⃣", "🔟"];
		let choices = [
			"zero",
			"one",
			"two",
			"three",
			"four",
			"five",
			"six",
			"seven",
			"eight",
			"nine",
			"ten",
			"11",
			"12",
			"13",
			"14",
			"15",
		];
		let possibilities = args.slice(1).join(" ").split("|");

		embed
			.setColor("#3333cc")
			.setTitle("Veuillez choisir __une__ réponse")
			.setAuthor(`${bot.user.username}`, bot.user.displayAvatarURL)
			.setFooter(
				`Demandé par ${message.author.tag}`,
				message.author.displayAvatarURL
			)
			.setTimestamp();

		for (let i = 0; i < possibilities.length; i++) {
			let tmpTitle = `${i} - ${possibilities[i]}\r\n`;
			let tmpValue = "¯¯¯";
			for (let j = 0; j < possibilities[i].length; j++) {
				tmpValue += "¯";
			}
			embed.addField(tmpTitle, tmpValue);
		}
		const msg = message.channel.send(embed);
		for (let i = 0; i < possibilities.length; i++) {
			// msg.react(choices[i]);
			msg.react(
				msg.guild.emojis.cache.find(
					(emoji) => emoji.name === ":" + choices[i] + ":"
				)
			)
				.then(console.log)
				.catch(console.error);
		}
	} else if (
		["anonyme", "anonymous", "blind", "anon", "aveugle"].includes(args[0])
	) {
		message.channel.send("Le sondage anonyme n'est pas encore implémenté");
	} else {
		message.channel.send("Type de poll invalide.");
	}
	return message.delete();
};

module.exports.help = {
	name: "poll",
	description:
		"Commande pour faire un sondage à choix multiples ou à choix binaire",
	usage: "(type - ouinon/binaire OU choix/liste) [anonyme/aveugle] (args[0]|args[1]|args[2])",
	category: "Utils",
	aliases: ["poll", "sondage", "ask", "demande"],
};
