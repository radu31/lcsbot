module.exports.run = async (bot, message, args) => {
	message.member.voice.channel.join();
	return message.delete();
};

module.exports.help = {
	name: "join",
	description: "Commande pour dire au bot de rejoindre le channel [spécifié]",
	usage: "[channel]",
	category: "Utils",
	aliases: ["rejoint", "rejoins"],
};
