// eslint-disable-next-line no-unused-vars
module.exports.run = async (bot, message, args) => {
	const Discord = require("discord.js");

	if (!message.mentions.users.size && !args) {
		message.channel.send(
			`Ton avatar: <${message.author.displayAvatarURL({ size: 1024 })}>`
		);
	} else if (message.mentions.users.size) {
		const avatarList = message.mentions.users.map((user) => {
			bot.users.fetch(user.id).then((user) => {
				const embed = new Discord.MessageEmbed()
					.setColor("#3333cc")
					.setTitle(
						`Avatar ` +
							(["a", "e", "i", "o", "u", "y"].includes(
								user.username[0].toLowerCase()
							)
								? "d'"
								: "de") +
							` ${user.username}:`
					)
					.setImage(user.avatarURL({ size: 1024 }))
					.setAuthor(
						`${bot.user.username}`,
						bot.user.displayAvatarURL()
					)
					.setFooter(
						`Demandé par  ${message.author.tag}`,
						message.author.displayAvatarURL()
					);
				return message.channel.send(embed);
			});
		});
	} else if (args.length === 1) {
		bot.users.fetch(args[0]).then((user) => {
			const embed = new Discord.MessageEmbed()
				.setColor("#3333cc")
				.setTitle(
					`Avatar ` +
						(["a", "e", "i", "o", "u", "y"].includes(
							user.username[0].toLowerCase()
						)
							? "d'"
							: "de") +
						` ${user.username}:`
				)
				.setImage(user.avatarURL({ size: 1024 }))
				.setAuthor(`${bot.user.username}`, bot.user.displayAvatarURL())
				.setFooter(
					`Demandé par  ${message.author.tag}`,
					message.author.displayAvatarURL()
				);
			return message.channel.send(embed);
		});
	} else if (message.mentions.users.size > 1) {
		const avatarList = message.mentions.users.map((user) => {
			return (
				`Avatar ` +
				(["a", "e", "i", "o", "u", "y"].includes(
					user.username[0].toLowerCase()
				)
					? "d'"
					: "de") +
				` ${user.username}:` +
				` <${user.avatarURL}>`
			);
		});
		return message.channel.send(avatarList);
	}
};

module.exports.help = {
	name: "avatar",
	description: "Commande pour voir l'avatar de quelqu'un",
	usage: "[IdUtilisateur ou MentionUtilisateur ou listeDeMentions]",
	category: "Utils",
	aliases: ["a", "pp", "picture", "pic", "upic", "photo"],
};
