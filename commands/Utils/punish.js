module.exports.run = async (bot, message, args) => {
	if (!bot.config.owners.includes(message.author.id)) return; // admin command
	usr = message.mentions.members.first();
	if (args.length < 1 || args.length > 2) {
		message.reply(
			"Tu dois avoir au moins un argument (la mention) et au plus deux."
		);
	} else {
		var temps = 0;
		const timeoutSec = args.length < 2 ? 10 : args[1];

		setInterval(() => {
			if (temps <= timeoutSec) {
				temps++;
				// usr.voice.setChannel("687093546557505561")   // punition dans PyBot
				usr.voice
					.setChannel("687038214061424652") // punition dans BibiLand123
					.catch((err) => {
						if (
							err.message !==
							"Le membre n'est pas connecté à un channel vocal"
						) {
							console.log("erreur!? " + err);
						}
						console.log(
							"l'usr est parti avant d'être move lmao scrub"
						);
					});
			}
		}, 1000);
		message.channel
			.send(
				"<@" +
					usr.id +
					">" +
					", une punition de " +
					timeoutSec +
					" secondes s'est abbatue sur toi, et c'est <@" +
					message.author.id +
					"> qui l'a invoquée."
			)
			.catch((err) => console.log("erreur: " + err));
	}
	message.delete();
};

module.exports.help = {
	name: "punish",
	description:
		"Commande pour punir un utilisateur pendant une durée spécifiée",
	usage: "punish (mention) [temps (défaut = 10sec)]",
	category: "Utils",
	aliases: ["punir", "gtfo", "sit", "assis"],
};
