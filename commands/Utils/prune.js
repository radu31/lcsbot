module.exports.run = async (bot, message, args) => {
	let amount = parseInt(args[0]) + 1;
	if (amount > 50) amount = amount - 5;

	if (isNaN(amount)) {
		return message.reply("C'est pas un nombre valide");
	} else if (amount < 1) {
		return message.reply(
			"Ton nombre doit être positif, " + message.author.username
		);
	} else if (amount > 100 - 1) {
		message.channel.send(
			"Faut que ton paramètre soit inférieur à ça, mon gars"
		);
	} else {
		message.channel.bulkDelete(amount - 3, true).catch((err) => {
			console.error(err);
			message.channel.send("Y'a eu une erreur coline de bines");
		});
	}
};

module.exports.help = {
	name: "prune",
	description: "Commande pour effacer un certain nombre de messages",
	usage: "(nbMessages)",
	category: "Utils",
	aliases: ["rm", "del", "delete", "effacer", "efface", "purge"],
};
