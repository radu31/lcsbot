const Discord = require("discord.js");
var horaires = require("../../utils/horaires");
const fs = require("fs");
// const horaires = fs.readFileSync("utils/horaires.json", "utf8");

// ajouter: yo horaire add lundi 8h00 10:50|14h00|16:00 18h00&mercredi 10h00 12h00|15h00 18:00
// arg[0]=add | arg[1]=lundi | arg[2]=8:00 | arg[3]=10h00
module.exports.run = async (bot, message, args) => {
	const usrid = message.author.id;
	const joursSemaine = [
		"Dimanche",
		"Lundi",
		"Mardi",
		"Mercredi",
		"Jeudi",
		"Vendredi",
		"Samedi",
	];
	// nouvelle entrée
	if (args[0] == "ajouter" || args[0] == "add") {
		var jours = args.splice(1).toString().split("&");

		// nouvelle entrée de jour(s)/cour(s)
		if (!horaires.hasOwnProperty(usrid)) {
			horaires[usrid] = {
				exceptions: ["Dimanche", "Samedi"],
				jours: [],
			};
			joursSemaine.forEach((j) => {
				horaires[usrid]["jours"].push({
					jour: j,
					cours: [],
				});
			});
			// nouvelle entrée de personne avec cours
		}

		jours.forEach((j) => {
			var jour, jourSem, no;
			j.split("|").forEach((c) => {
				var coursTmp = c.split(",");
				if (
					joursSemaine.includes(
						coursTmp[0][0].toUpperCase() + coursTmp[0].substr(1)
					)
				) {
					jour = coursTmp[0][0].toUpperCase() + coursTmp[0].substr(1);
					coursTmp = coursTmp.splice(1);
					jourSem = joursSemaine.indexOf(jour);
					no = 1;
				} else {
					no = 2;
				}
				if (!joursSemaine.includes(jour)) {
					return message.reply(
						"t'as mal écrit un de tes jours coline de bines"
					);
				}
				horaires[usrid]["jours"][jourSem]["cours"].push({
					numéro: no,
					début: coursTmp[0].replace("h", ":"),
					fin:
						coursTmp.length == 2
							? coursTmp[1].replace("h", ":")
							: "",
				});
			});
		});

		fs.writeFileSync(
			"utils/horaires.json",
			JSON.stringify(horaires, null, 4)
		);
	} else if (args[0] == "remove" || args[0] == "supprimer") {
		horaires[usrid].jours.forEach((j) => {
			if (j.jour == args[1][0].toUpperCase() + args[1].substr(1)) {
				j.cours.forEach((c) => {
					if (c.numéro == args[2])
						j.cours.splice(j.cours.indexOf(c), 1);
					else message.reply("yo, le numéro de ton cours existe pas");
				});
			} else
				message.reply(
					"yo, le nom du jour de ton cours existe pas coline de bines"
				);
		});
	} else if (args.length == 0) {
		// horaire pour la semaine
		// var tmpd = new Date();
		// var demain = joursSemaine[tmpd.setDate(tmpd.getDate() + 1).getDay];
		const embed = new Discord.MessageEmbed();

		if (!horaires.hasOwnProperty(usrid))
			return message.reply("t'es pas dans ma base de données, vas t'en");
		else {
			horaires[usrid].jours.forEach((j) => {
				embed
					.setColor("#3333cc")
					.setTitle(`yo, voici tes cours: `)
					.setAuthor(
						`${bot.user.username}`,
						bot.user.displayAvatarURL()
					)
					.setFooter(
						`Demandé par ${message.author.tag}`,
						message.author.displayAvatarURL()
					);
				if (!horaires[usrid].exceptions.includes(j.jour)) {
					var tempStr = "";
					j.cours.forEach((c) => {
						// embed.addField(`__**Cours #${c.numéro}:**__`, `__**${c.début}${c.fin.length>0?` - ${c.fin}`:""}**__`, false);
						tempStr += `**Cours #${c.numéro}:**\r\n${c.début}${
							c.fin.length > 0 ? ` - ${c.fin}` : ""
						}\r\n`;
					});
					embed.addField(
						`__**${j.cours.length} cours ${j.jour}:**__`,
						tempStr,
						true
					);
				}
			});
			message.channel.send(embed);
		}
	} else if (args[0] == "demain") {
		const embed = new Discord.MessageEmbed();
		var tmpd = new Date();
		tmpd.setDate(tmpd.getDate() + 1);
		// var demain = joursSemaine[tmpd.setDate(tmpd.getDate() + 1).getDay];
		var demain = joursSemaine[tmpd.getDay()];

		// var demain = joursSemaine[tmp];
		if (!horaires.hasOwnProperty(usrid))
			return message.reply("t'es pas dans ma base de données, vas t'en");
		else {
			horaires[usrid].jours.forEach((j) => {
				if (j["jour"] == demain && j["cours"].length > 0) {
					embed
						.setColor("#3333cc")
						.setTitle(`yo, t'as ${j["cours"].length} cours demain:`) //j["cours"].length
						.setAuthor(
							`${bot.user.username}`,
							bot.user.displayAvatarURL()
						)
						.setFooter(
							`Demandé par ${message.author.tag}`,
							message.author.displayAvatarURL()
						);
					j["cours"].forEach((c) => {
						embed.addField(
							`__**Cours #${c.numéro}:**__`,
							`**${c.début}${
								c.fin.length > 0 ? ` - ${c.fin}` : ""
							}**`,
							true
						);
					});
					message.channel.send(embed);
				}
			});
		}
	}
};

// ajouter: yo horaire add lundi 8h00 10:50|14h00|16:00 18h00&mercredi 10h00 12h00|15h00 18:00
// arg[0]=add | arg[1]=lundi | arg[2]=8:00 | arg[3]=10h00
module.exports.help = {
	name: "horaire",
	description: "Commande pour afficher l'horaire",
	usage: "[add / ajouter (jour ex: lundi *Si un jour n'est pas nommé, il n'y aura aucun cours prévu pour ce jour) (heure du début du cours ex: 8:00 ou 8h00) [heure de fin du cours ex: 11:50 ou 10h50] pour ajouter plusieurs cours dans cette journée, **ajouter '|'** et répeter la séquence d'arguments à partir de l'heure de début. Pour ajouter plus de jours **ajouter '&'** ex: add lundi 8h00|12h00 15h00&mercredi 8h00 12h00|16h00] [remove/delete/enlever/supprimer (nom du jour) (# du cours)] [semaine] [demain]",
	category: "Utils",
	aliases: [],
};
