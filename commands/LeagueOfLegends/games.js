const Discord = require("discord.js");
const fs = require("fs");
const config = require("../../config");
const champs = fs.readFileSync("utils/championsIdToName.json");
const joueurs = require("../../utils/webhookppl.json");
const k = require("kayn");
const myCache = new k.LRUCache({ max: 5 });
const kayn = k.Kayn(config.ritokey)({
	region: "na",
	debugOptions: {
		isEnabled: true,
		showKey: false,
	},
	requestOptions: {
		shouldRetry: true,
		numberOfRetriesBeforeAbort: 3,
		delayBeforeRetry: 1000,
	},
	cacheOptions: {
		cache: new k.BasicJSCache(),
	},
});

module.exports.run = async (bot, message, args) => {
	if (args.length < 1)
		return message.reply(
			"Votre commande est invalide. " +
				"Veuillez réessayer. Utilisez *yo help games*."
		);
	// embed object
	const embed = new Discord.MessageEmbed();
	// les 100 dernières parties
	var matches;
	// les trois dernières parties
	var games = new Array(3);
	// stats des trois dernières parties
	var gamesStats = new Array(3);
	// 3 champions joués
	var playedChampNames = new Array(3);
	// to know if args.join(" ") exists in saved json summs
	var sumIdInJson = "";

	embed
		.setColor("#3333cc")
		.setTitle(
			`yo tous les fdp regardez comme ${args.join(
				" "
			)} est bon, voici ses trois dernières parties:`
		)
		.setAuthor(`${bot.user.username}`, bot.user.displayAvatarURL())
		.setFooter(
			`Demandé par  ${message.author.tag}`,
			message.author.displayAvatarURL()
		);

	// games list (to index 100)
	// (last) object game from matches (only for gameId)
	for (var i = 0; i < joueurs.ppl.length; i++) {
		if (args.join(" ") == joueurs.ppl[i].summonerName) {
			sumIdInJson = joueurs.ppl[i].id;
		}
	}

	if (sumIdInJson) {
		try {
			// 100 dernières parties
			matches = await kayn.Matchlist.by.accountID(sumIdInJson);
		} catch (error) {
			console.log("oopsie ", error);
		}
	} else {
		try {
			// 100 dernières parties
			matches = await kayn.Matchlist.by.accountID(
				await (
					await kayn.Summoner.by.name(args.join(" "))
				).accountId
			);
		} catch (error) {
			console.log("fuck ", error);
		}
	}

	// les trois dernières parties
	try {
		for (var i = 0; i < 3; i++) {
			games[i] = matches.matches[i];
		}
	} catch (error) {
		console.log("tabarnak ", error);
	}

	// get stats pour dernières 3 parties
	for (var y = 0; y < 3; y++) {
		var match, tmpParticipantId;
		try {
			match = await kayn.Match.get(games[y].gameId);
		} catch (error) {
			console.log("calis ", error);
		}
		// identification du summoner dans le json [1;10]
		for (var k = 0; k < match.participantIdentities.length; k++) {
			if (
				match.participantIdentities[k].player.summonerName ==
				args.join(" ")
			) {
				tmpParticipantId = match.participantIdentities[k].participantId;
			}
		}
		gamesStats[y] = match.participants[tmpParticipantId - 1].stats;
		playedChampNames[y] = JSON.parse(champs, (key, value) => {
			return value;
		})[match.participants[tmpParticipantId - 1].championId];
	}

	for (var x = 0; x < 3; x++) {
		const pentas = gamesStats[x].pentaKills,
			quadras = gamesStats[x].quadraKills,
			kills = gamesStats[x].kills,
			deaths = gamesStats[x].deaths,
			assists = gamesStats[x].assists,
			dmgToChamps = gamesStats[x].totalDamageDealtToChampions,
			visionScore = gamesStats[x].visionScore,
			wardsPlaced = gamesStats[x].wardsPlaced,
			wardsKilled = gamesStats[x].wardsKilled,
			minions = gamesStats[x].totalMinionsKilled,
			firstBlood =
				gamesStats[x].firstBloodKill || gamesStats[x].firstBloodAssist,
			firstTower =
				gamesStats[x].firstTowerKill || gamesStats[x].firstTowerAssist,
			campsTeam = gamesStats[x].neutralMinionsKilledTeamJungle,
			campsEnnemi = gamesStats[x].neutralMinionsKilledEnemyJungle,
			lePlusTempsEnVie = gamesStats[x].longestTimeSpentLiving,
			dommagesAuxObjectifs = gamesStats[x].damageDealtToObjectives,
			dommagesAuxTours = gamesStats[x].damageDealtToTurrets,
			tempsCCautres = gamesStats[x].timeCCingOthers;

		var str = "";
		if (pentas != 0) str += "**Penta**" + "\r\n";
		if (quadras != 0) str += "**Quadra**" + "\r\n";
		if (firstBlood) str += "**First Blood**" + "\r\n";
		if (firstTower) str += "**First Tower**" + "\r\n";
		if ((kills + assists) / deaths > 4)
			str += "**Beau KDA** → " + (kills + assists) / deaths + "\r\n";
		str += "**KDA** → " + `${kills} / ${deaths} / ${assists}` + "\r\n";
		str +=
			"**Le plus de temps passé en vie** → " +
			lePlusTempsEnVie +
			"sec\r\n";
		str += "**DMG to champs** → " + dmgToChamps + "\r\n";
		if (visionScore > 30)
			str += "**Vision Score** → " + visionScore + "\r\n";
		if (wardsPlaced > 10)
			str += "**Wards placées** → " + wardsPlaced + "\r\n";
		if (wardsKilled > 4) str += "**Wards tuées** → " + wardsKilled + "\r\n";
		if (minions > 60) str += "**CS** → " + minions + "\r\n";
		if (campsTeam > 40)
			str += "**Camps tués dans la jungle amie** → " + campsTeam + "\r\n";
		if (campsEnnemi > 10)
			str +=
				"**Camps tués dans la jungle ennemie** → " +
				campsEnnemi +
				"\r\n";
		if (dommagesAuxObjectifs > 5000)
			str +=
				"**Dommages faits aux objectifs** → " +
				dommagesAuxObjectifs +
				"\r\n";
		if (dommagesAuxTours > 5000)
			str +=
				"**Dommages faits aux tours** → " + dommagesAuxTours + "\r\n";
		if (tempsCCautres > 30)
			str +=
				"**Temps passé à CC les autres** → " +
				dommagesAuxTours +
				"sec\r\n";

		embed.addField(`__**${playedChampNames[x]}**__`, str, false);
	}

	await message.channel.send(embed);
	return message.delete();
};

module.exports.help = {
	name: "games",
	description:
		"Commande pour voir les 3 dernières parties jouées et les informations associées à ces dernières du summoner [spécifié (ex: Greenblud OU hide on bush)]",
	usage: "(summoner)",
	category: "LeagueOfLegends",
	aliases: ["history", "historique", "matches", "lastgames"],
};
