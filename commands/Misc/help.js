const { readdirSync } = require("fs");
const Discord = require("discord.js");

module.exports.run = (bot, message, args) => {
	const embed = new Discord.MessageEmbed()
		.setColor("#2C2F33")
		.setAuthor(`${bot.user.username} Help`, bot.user.displayAvatarURL)
		.setFooter(
			`Demandé par ${message.author.tag}`,
			message.author.displayAvatarURL
		)
		.setTimestamp();
	if (args[0]) {
		let command = args[0];
		let cmd;
		if (bot.commands.has(command)) {
			cmd = bot.commands.get(command);
		} else if (bot.aliases.has(command)) {
			cmd = bot.commands.get(bot.aliases.get(command));
		}
		if (!cmd)
			return message.channel.send(
				embed
					.setTitle("Commande invalide.")
					.setDescription(
						`Faites \`${bot.config.prefix}help\` pour la liste de toutes les commandes.`
					)
			);
		command = cmd.help;
		embed.setTitle(
			`${
				command.name.slice(0, 1).toUpperCase() + command.name.slice(1)
			} commande help`
		);
		embed.setDescription(
			[
				`❯ **Commande:** ${
					command.name.slice(0, 1).toUpperCase() +
					command.name.slice(1)
				}`,
				`❯ **Description:** ${
					command.description || "Pas de description."
				}`,
				`❯ **Usage:** ${
					command.usage
						? `\`${bot.config.prefix}${command.name} ${command.usage}\``
						: "Pas d'usage"
				} `,
				`❯ **Alias:** ${
					command.aliases ? command.aliases.join(", ") : "None"
				}`,
				`❯ **Catégorie:** ${
					command.category ? command.category : "General" || "Misc"
				}`,
			].join("\n")
		);

		return message.channel.send(embed);
	}
	const categories = readdirSync("./commands/");
	embed.setDescription(
		[
			`Commandes disponibles pour ${bot.user.username}.`,
			`Le préfixe du bot est **${bot.config.prefix}**`,
			"`()` est requis et `[]` est optionnel",
		].join("\n")
	);
	categories.forEach((category) => {
		const dir = bot.commands.filter(
			(c) => c.help.category.toLowerCase() === category.toLowerCase()
		);
		const capitalise =
			category.slice(0, 1).toUpperCase() + category.slice(1);

		try {
			if (dir.size === 0) return;
			if (bot.config.owners.includes(message.author.id))
				embed.addField(
					`❯ ${capitalise}`,
					dir.map((c) => `\`${c.help.name}\``).join(", ")
				);
			else if (category !== "Developer")
				embed.addField(
					`❯ ${capitalise}`,
					dir.map((c) => `\`${c.help.name}\``).join(", ")
				);
		} catch (e) {
			console.log(e);
		}
	});
	return message.channel.send(embed);
};

module.exports.help = {
	name: "help",
	aliases: ["h", "aide"],
	description: "Montre les commandes disponibles",
	usage: "(commande)",
	category: "Misc",
};
