module.exports.run = async (bot, message, args) => {
	if (!bot.config.owners.includes(message.author.id)) return;
	message.mentions.members.first().send(args.slice(1).join(" "));
	return message.delete();
};

module.exports.help = {
	name: "message",
	description: "Commande pour envoyer un message privé à (mention)",
	usage: "(mention)",
	category: "Misc",
	aliases: ["mess"],
};
