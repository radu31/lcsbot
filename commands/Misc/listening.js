module.exports.run = async (bot, message, args) => {
	if (!bot.config.owners.includes(message.author.id)) return;
	bot.user
		.setActivity(args.join(" "), { type: "LISTENING" })
		.then((presence) =>
			console.log(`Activité est ${presence.activities[0].name}`)
		)
		.catch(console.error);
	return message.delete();
};

module.exports.help = {
	name: "listening",
	description: 'Commande pour changer la présence "*listening*" du bot',
	usage: "(activité)",
	category: "Misc",
	aliases: ["écoute", "ecoute", "listen", "écouter", "ecouter"],
};
