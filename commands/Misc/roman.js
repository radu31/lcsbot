module.exports.run = async (bot, message, args) => {
	var lookup = {
			M: 1000,
			CM: 900,
			D: 500,
			CD: 400,
			C: 100,
			XC: 90,
			L: 50,
			XL: 40,
			X: 10,
			IX: 9,
			V: 5,
			IV: 4,
			I: 1,
		},
		roman = "",
		tmp = "",
		i;
	tmp = args[0];
	for (i in lookup) {
		while (tmp >= lookup[i]) {
			roman += i;
			tmp -= lookup[i];
		}
	}
	message.reply(`${args[0]} --> ${roman}`);
	return message.delete();
};

module.exports.help = {
	name: "roman",
	description: "Commande pour convertir un nombre arabique en romain",
	usage: "(nombre)",
	category: "Misc",
	aliases: ["romanize", "rom"],
};
