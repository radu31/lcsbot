var quotes = require("../../utils/quotes");
const fs = require("fs");
const quotesfs = fs.readFileSync("utils/quotes.json");

module.exports.run = async (bot, message, args) => {
	var usrname = "";
	var année = new Date().getFullYear();

	// nouvelle entrée
	if (args[0] == "new" || args[0] == "add") {
		var usrid = args[1].slice(3, 21);

		// nouvelle entrée de citation
		if (quotes.hasOwnProperty(usrid)) {
			quotes[usrid].quotes.push({
				quote: args.slice(2).join(" "),
				year: année,
			});
			fs.writeFileSync(
				"utils/quotes.json",
				JSON.stringify(quotes, null, 4)
			);
			// nouvelle entrée de personne avec citation
		} else {
			quotes[usrid] = {
				name: args[2],
				quotes: [],
			};
			quotes[usrid].quotes.push({
				quote: args.slice(3).join(" "),
				year: année,
			});
			fs.writeFileSync(
				"utils/quotes.json",
				JSON.stringify(quotes, null, 4)
			);
		}
		// avoir toutes les citations d'une personne
	} else if (args[0] == "get" && args[1] == "all") {
		var existe = false,
			tmpusrid = args[2].slice(3, 21);
		const getnom = JSON.stringify(quotes[tmpusrid].name).slice(
			1,
			quotes[tmpusrid].name.length + 1
		);
		for (const k in quotes) {
			if (k == tmpusrid) {
				existe = true;
			}
		}
		if (existe) {
			var cita = quotes[tmpusrid].quotes,
				espaces = "    ";
			var cits = [];
			cita.forEach((q) => {
				for (let i = 0; i < q.quote.length; i++) {
					espaces += " ";
				}
				cits.push(
					`*"${q.quote}"*\r\n${espaces}**-${getnom} ${q.year}**`
				);
				espaces = "   ";
			});
			message.channel.send(cits);
		} else {
			message.reply("Erreur");
		}
		// avoir une citation aléatoire d'une personne (avec get ou sans)
	} else {
		var tmpid;
		isNaN(args[0][4]) ? (tmpid = args[1]) : (tmpid = args[0]);
		for (const k in quotes) {
			if (k == tmpid.slice(3, 21)) {
				var espaces = "    ";
				const getnom = JSON.stringify(quotes[k].name).slice(
					1,
					quotes[k].name.length + 1
				);
				// console.log("NOM DU USER DANS JSON = ", JSON.stringify(quotes[k].name));
				const randquote =
					quotes[k].quotes[
						Math.floor(Math.random() * quotes[k].quotes.length)
					];
				for (let i = 0; i < randquote.quote.length; i++) {
					espaces += " ";
				}
				message.channel.send(
					`*"${randquote.quote}"*\r\n${espaces}**-${getnom} ${randquote.year}**`
				);
				espaces = "   ";
			}
		}
	}
};

module.exports.help = {
	name: "quote",
	description:
		"Commande pour ajouter ou afficher une (citation aléatoire) ou des (toutes les citations d'une personne existante) citations",
	usage: "[new OU get] (mention) ([nom/surnom] *obligatoire si l'utilisateur n'éxiste pas dans la base de données*) [citation]",
	category: "Utils",
	aliases: ["cit", "quo", "quotes", "citation"],
};
