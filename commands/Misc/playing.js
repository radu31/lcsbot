module.exports.run = async (bot, message, args) => {
	if (!bot.config.owners.includes(message.author.id)) return;
	bot.user
		.setActivity(args.join(" "), { type: "PLAYING" })
		.then((presence) =>
			console.log(`Activité est ${presence.activities[0].name}`)
		)
		.catch(console.error);
	return message.delete();
};

module.exports.help = {
	name: "playing",
	description: 'Commande pour changer la présence "*playing*" du bot',
	usage: "(activité)",
	category: "Misc",
	aliases: ["play", "joue", "game", "jeu"],
};
