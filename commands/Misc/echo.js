module.exports.run = async (bot, message, args) => {
	if (!bot.config.owners.includes(message.author.id)) return;
	if (!args.length)
		return message.reply(
			"veuillez spécifier l'id d'un canal de destination."
		);

	const channel = bot.channels.get(args[0]);
	if (channel) {
		if (args[1] === "stfu") {
			channel.send(args.slice(2));
		} else {
			channel.send(args.slice(1));
			return message.reply(
				`J'ai envoyé un message au channel \`${args[0]}\`!`
			);
		}
	} else {
		return message.reply(
			`Tu dois spécifier l'ID du channel.. cmon man \`${args[0]}\`!`
		);
	}
};

module.exports.help = {
	name: "echo",
	description: "Commande pour envoyer un message en tant que bot",
	usage: "(channel) [stfu] (args)",
	category: "Misc",
	aliases: [],
};
