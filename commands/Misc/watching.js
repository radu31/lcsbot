module.exports.run = async (bot, message, args) => {
	if (!bot.config.owners.includes(message.author.id)) return;
	bot.user
		.setActivity(args.join(" "), { type: "WATCHING" })
		.then((presence) =>
			console.log(`Activité est ${presence.activities[0].name}`)
		)
		.catch(console.error);
	return message.delete();
};

module.exports.help = {
	name: "watching",
	description: 'Commande pour changer la présence "*watching*" du bot',
	usage: "(activité)",
	category: "Misc",
	aliases: ["watch", "regarde"],
};
