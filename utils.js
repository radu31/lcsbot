function findEmoji(id) {
	const temp = this.emojis.get(id);
	if (!temp) return null;

	const emoji = Object.assign({}, temp);
	if (emoji.guild) emoji.guild = emoji.guild.id;
	emoji.require_colons = emoji.requiresColons;

	return emoji;
}

function uptime(millis) {
	return Math.floor(millis / 60000);
}
