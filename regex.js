module.exports = {
	mentions: {
		userOrMember: /^(?:<@!?)?(\d{17,19})>?$/,
		channel: /^(?:<#)?(\d{17,19})>?$/,
		role: /^(?:<@&)?(\d{17,19})>?$/,
		snowflake: /^(\d{17,19})$/,
	},
	misc: {
		emoji: /^(?:<a?:\w{2,32}:)?(\d{17,19})>?$/,
		username: /.{2,32}/,
		invite: /(http(s)?:\/\/)?(www\.)?(discord\.(gg|li|me|io)|discordapp\.com\/invite|invite\.gg)\/.\w+/,
		tag: /(#)\d{4}/,
		usertag: /.{2,32}(#)\d{4}/,
		token: /[\w]{24}\.[\w]{6}\.[\w-_]{27}/,
	},
};

/**
You can find the details how these regex works on https://regex101.com/
If you have issues with these and if you know better way to do it please drop a comment :D
**/
