# lcsbot

Bot Discord.js avec implémentation de l'API Riot Games.

Avant tout, créez un fichier config.js dans le dossier racine avec le contenu suivant:

```js
module.exports = {
	prefix: "yo ",
	owners: [
		"275428645076467715"	// moi
	],
	token: "aaaaaaaaaaaaaaaaa.bbbbb.cccc-ddddddddddddddddddddddddddddd",
	ritokey: "RGAPI-12345678-9012-3456-7890-123456789901",
	ritohost: "na1.api.riotgames.com",
	ddragonhost: "ddragon.leagueoflegends.com"
};
```

# À changer
- owners:     identifiant discord
- token:      [discord bot token](https://discord.com/developers/applications)
- ritokey:    [riot api key](https://developer.riotgames.com/)

# Profitez!
- npm install
- npm start
